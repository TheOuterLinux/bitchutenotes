https://bitchute.com/channel/theouterlinux

Albums
      |
      + Command-Line
		    |
                    + w3m+mpv=magic (https://www.bitchute.com/video/iuVQrGO3yeRz)
                    + Twitch - The MPV, the IRSSI, and the TMUX (https://www.bitchute.com/video/ly4YNCZtSswb)
                    + Encrypted Communication (https://www.bitchute.com/video/6fqO3XeoBLq5)
                    + Recording Music - (https://www.bitchute.com/video/abnIpd8vxWnZ)
                    + Spooky Math - Solve Halloween themed math problems using the command line (https://www.bitchute.com/video/zbgQc6eoodO9)
                    + Image editing in the command line (https://www.bitchute.com/video/KAvvdoMXtdRq/)
                    + Composing Music (https://www.bitchute.com/video/MfPKZfOazg5f/)
                    + Translator (https://www.bitchute.com/video/yYyZWUtIEuG5/)
                    + Playing BitChute videos from the command-line (https://www.bitchute.com/video/W4g3Jktibucb)
                    + "I Love Linux" ft. eSpeak (https://www.bitchute.com/video/J3TpLzRbTNea)
                    + Fireplace (https://www.bitchute.com/video/6xWf1jeaXc11)
                    + StreamPi - FFMPEG for RTMP Live Streaming (https://www.bitchute.com/video/2yWzuFMUaBra)
                    + Social Script (https://www.bitchute.com/video/UT4tqkXvqcAr)
                    + StreamPi Update #1 (https://www.bitchute.com/video/iqlQK8eogtNw/)
        |
        + GUI
             |
                    + StreamPi Update 02: StreamPi has a GUI! (https://www.bitchute.com/video/MM2YcMYnqldc/)
                    + PsychOS 3: Insanity (https://www.bitchute.com/video/GdV0aVQ43mkP/)
